from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.core.urlresolvers import reverse

from accounts.views import login_view, register_view, logout_view, redirect_root


urlpatterns = [

    url(r'^register/', register_view, name='register'),
    url(r'^login/', login_view, name='login'),
    url(r'^logout/', logout_view, name='logout'),
    url(r'^$', redirect_root)
]
